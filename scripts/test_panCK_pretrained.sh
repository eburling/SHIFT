set -ex
python test.py --dataroot ./datasets/SHIFT_dataset --name panCK_pretrained --model pix2pix --netG unet_256 --ngf 128 --output_nc 1 --direction AtoB --dataset_mode unaligned --norm batch --eval
